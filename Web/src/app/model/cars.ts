export class ViewCar {
    carID: number;
    name: string;
    model: string;
    maker: string;
    client: string;
    year: number;
}

export class Cars {
    Model: string;
    Maker: string;
    Year: number;
    ClientID: number;
}

export class Detailcars {
    carID: number;
    name: string;
    model: string;
    maker: string;
    client: string;
    year: number;
    historyReparation: reparation[]
}

export class reparation {
    carRepairID: number;
    departure: Date;
    description: string;
    entry: Date;
    workShopID: Number;
    carID: number;
}