export class ClientView {
    clientID: number;
    workshopClientID: number;
    name: string;
    lastName: string;
    workShop: string;
}

export class Clients {
    ClientID: number;
    Name: string;
    LastName: string;
    WorkShopID: number;
}

export class Workshop {
    name: string;
    workShopID: number;
}

export class ViewClients {
    clientID: number;
    fullName: string;

}