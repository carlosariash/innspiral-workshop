export class CarRepairView {
    carRepairID: number;
    client: string;
    departure: Date;
    description: string;
    entry: Date;
    nameCar: string;
}