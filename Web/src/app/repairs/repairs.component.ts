import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { CarRepairView } from '../model/repairs';
import { RepairsService } from '../services/repair.service';
import * as Enumerable from 'linq-es2015';

@Component({
  selector: 'app-repairs',
  templateUrl: './repairs.component.html',
  styleUrls: ['./repairs.component.scss']
})
export class RepairsComponent implements OnInit {

  CarRepairViews: CarRepairView[] = [];

  constructor(private RepairsService: RepairsService) { }

  ngOnInit() {
    this.GetAllRepairs();
  }

  GetAllRepairs() {
    this.RepairsService
      .getAllRepair()
      .pipe(first())
      .subscribe((CarRepairViews) => {
        this.CarRepairViews = Enumerable.asEnumerable(CarRepairViews).OrderByDescending(u=> u.entry).ToArray();
      });
  }

}
