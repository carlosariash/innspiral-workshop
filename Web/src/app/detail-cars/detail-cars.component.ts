import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { first } from 'rxjs/operators';
import { Detailcars, reparation } from '../model/cars';
import { Workshop } from '../model/Clients';
import { CarsService } from '../services/cars.service';
import { ClientsService } from '../services/clients.service';
import notify from 'devextreme/ui/notify';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-detail-cars',
  templateUrl: './detail-cars.component.html',
  styleUrls: ['./detail-cars.component.scss']
})
export class DetailCarsComponent implements OnInit {

  Detailcars: Detailcars = new Detailcars();
  reparations: reparation[] = [];
  Id: string;
  modalRef: BsModalRef;
  reparation: reparation;
  Workshops: Workshop[] = [];
  now: Date;

  constructor(private route: ActivatedRoute, private CarsService: CarsService, private modalService: BsModalService, 
    private ClientsService: ClientsService) {

    this.route.queryParams.subscribe(params => {
      if (params['v'] !== undefined) {
        if (params['v'] !== undefined) {
          this.Id = params['v'];
        }
      }
    });
   }

  ngOnInit() {

    this.GetDetailCars();
  }

  GetDetailCars() {
    this.CarsService.GetDetailCars(+this.Id).pipe(first()).subscribe(DetailCar => {
      this.Detailcars = DetailCar;
      this.reparations = this.Detailcars.historyReparation;
    });
  }

  newreparations(template: TemplateRef<any>) {
    this.reparation = new reparation();
    this.now =  new Date();
    this.reparation.entry = this.now;
    this.GetAllWorkShop();
    this.modalRef = this.modalService.show(template, { class: 'modal-lg' });

  }

  GetAllWorkShop() {
    this.ClientsService
      .getAllWorkShop()
      .pipe(first())
      .subscribe((Workshops) => {
        this.Workshops = Workshops;
      });
  }

  SaveReparation(params) {
    const result = params.validationGroup.validate();
    if(result.isValid) {
      this.reparation.carID = +this.Id;
      this.CarsService.AddReparation(this.reparation).subscribe(
        data => {
          this.GetDetailCars();
            notify('Reparación guardado con exito', 'success', 800);
            return true;
        },
        error => {
          notify('Tuvimos un problema en almacenar la información', 'error', 800);
            return Observable.throw(error);
        }
    );
    this.modalRef.hide();


    }

  }

}
