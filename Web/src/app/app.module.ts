import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClientsComponent } from './clients/clients.component';
import { CarsComponent } from './cars/cars.component';
import { DxDataGridModule, DxTextBoxModule, DxSelectBoxModule, DxValidationSummaryModule,DxValidationGroupModule, DxValidatorModule, DxButtonModule,
  DxNumberBoxModule, DxTextAreaModule, DxDateBoxModule } from 'devextreme-angular';
import { ClientsService } from './services/clients.service';
import { HttpClientModule } from '@angular/common/http';
import { CarsService } from './services/cars.service';
import { RepairsComponent } from './repairs/repairs.component';
import { RepairsService } from './services/repair.service';
import { ModalModule } from 'ngx-bootstrap';
import { DetailCarsComponent } from './detail-cars/detail-cars.component';
import {AngularFontAwesomeModule} from 'angular-font-awesome';

@NgModule({
  declarations: [
    AppComponent,
    routingComponents
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DxDataGridModule,
    HttpClientModule,
    ModalModule.forRoot(),
    DxTextBoxModule,
    DxSelectBoxModule,
    DxValidationSummaryModule,
    DxValidationGroupModule,
    DxValidatorModule,
    DxButtonModule,
    AngularFontAwesomeModule,
    DxNumberBoxModule,
    DxTextAreaModule,
    DxDateBoxModule
    
  ],
  providers: [ClientsService, CarsService, RepairsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
