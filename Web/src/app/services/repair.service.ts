import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { CarRepairView } from '../model/repairs';
@Injectable()
export class RepairsService {

    public Server = environment.url;
    public GetAll = this.Server + 'Repair'
    constructor(private http: HttpClient) {}

    getAllRepair() {
      return this.http.get<CarRepairView[]>(this.GetAll);
    }

}