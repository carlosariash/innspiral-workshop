import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Clients, ClientView, Workshop, ViewClients } from '../model/Clients';
@Injectable()
export class ClientsService {

    private Server = environment.url;
    private GetAll = this.Server + 'Clients';
    private APIGetAllWorkShop = this.Server + 'Clients/GetAllWorkShop';
    private ApiSaveClients = this.Server + 'Clients';
    private APIGetAllClients = this.Server + 'Clients/GetAllClients';
    constructor(
        private http: HttpClient
      ) {}

    getAllClients() {
        return this.http.get<ClientView[]>(this.GetAll);
      }

    getAllWorkShop() {
      return this.http.get<Workshop[]>(this.APIGetAllWorkShop);

    }
    AddClient(item: Clients) {
      
      return this.http.post(this.ApiSaveClients, JSON.stringify(item), { headers: new HttpHeaders().set('Content-Type', 'application/json')});
    }

    getAllClient() {
      return this.http.get<ViewClients[]>(this.APIGetAllClients);
    }

}