import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Cars, Detailcars, reparation, ViewCar } from '../model/cars';
@Injectable()
export class CarsService {

    private Server = environment.url;
    private GetAll = this.Server + 'Cars'
    private SaveCars = this.Server + 'Cars'
    private GetCars = this.Server + 'Cars'
    private SaveReparation = this.Server + 'Repair'

    constructor(
        private http: HttpClient
      ) {}

    getAllCars() {
        return this.http.get<ViewCar[]>(this.GetAll);
      }
      AddCar(item: Cars) {
        return this.http.post(this.SaveCars, JSON.stringify(item), { headers: new HttpHeaders().set('Content-Type', 'application/json')});
      }
      
      GetDetailCars(Id: number){
        return this.http.get<Detailcars>(this.GetCars + '/' + Id);
      }
      AddReparation(item: reparation) {
        return this.http.post(this.SaveReparation, JSON.stringify(item), { headers: new HttpHeaders().set('Content-Type', 'application/json')});

      }

}