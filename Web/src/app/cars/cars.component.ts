import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { first } from 'rxjs/operators';
import { Cars, ViewCar } from '../model/cars';
import { ViewClients } from '../model/Clients';
import { CarsService } from '../services/cars.service';
import { ClientsService } from '../services/clients.service';
import notify from 'devextreme/ui/notify';
import { Observable } from 'rxjs';
import * as Enumerable from 'linq-es2015';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.scss']
})
export class CarsComponent implements OnInit {

  ViewCars: ViewCar[] = [];
  modalRef: BsModalRef;
  cars: Cars;
  ViewClients: ViewClients[] = [];

  constructor(private CarsService: CarsService, private modalService: BsModalService, private ClientsService: ClientsService) { }

  ngOnInit() {
    this.GetAllCarForsClients();
  }

  GetAllCarForsClients() {
    this.CarsService
      .getAllCars()
      .pipe(first())
      .subscribe((ViewCars) => {
        this.ViewCars = Enumerable.asEnumerable(ViewCars).OrderByDescending(u=> u.carID).ToArray(); ;;
      });
  }

  newCars(template: TemplateRef<any>) {
    this.cars = new Cars();
    this.GetAllClients();
    this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
  }

  GetAllClients() {
    this.ClientsService
    .getAllClient()
      .pipe(first())
      .subscribe((ViewClients) => {
        this.ViewClients = ViewClients;
        console.log(this.ViewClients);
      });
  }

  SaveCars(params) {
    const result = params.validationGroup.validate();
    if(result.isValid) {
      console.log(this.cars);
      this.CarsService.AddCar(this.cars).subscribe(
        data => {
          this.GetAllCarForsClients();
            notify('Automóvil guardado con exito', 'success', 800);
            return true;
        },
        error => {
          notify('Tuvimos un problema en almacenar la información', 'error', 800);
            return Observable.throw(error);
        }
    );
    this.modalRef.hide();

      
      
    }
  }

}
