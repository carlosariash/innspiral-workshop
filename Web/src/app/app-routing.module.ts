import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientsComponent } from './clients/clients.component';
import { CarsComponent } from './cars/cars.component';
import { RepairsComponent } from './repairs/repairs.component';
import { DetailCarsComponent } from './detail-cars/detail-cars.component';


const routes: Routes = [  
{ path: '', component: ClientsComponent, pathMatch: 'full' },
{ path: 'clients', component: ClientsComponent },
{ path: 'cars', component: CarsComponent },
{ path: 'repairs', component: RepairsComponent },
{ path: 'detailcar/:id', component: DetailCarsComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [ClientsComponent, CarsComponent, RepairsComponent, DetailCarsComponent];
