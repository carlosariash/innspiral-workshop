import { Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ClientsService } from '../services/clients.service';
import { first } from 'rxjs/operators';
import { Clients, ClientView, Workshop } from '../model/Clients';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import * as Enumerable from 'linq-es2015';
import notify from 'devextreme/ui/notify';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnInit {

  ClientViews: ClientView[] = [];
  modalRef: BsModalRef;
  Client: Clients;
  Workshops: Workshop[] = [];

  constructor(private ClientsService: ClientsService, private modalService: BsModalService) { }

  @ViewChild('template',{ static: true }) modal: ElementRef;

  ngOnInit() {
    this.GetAllClients();
  }

  SaveClients(params) {
    const result = params.validationGroup.validate();
    if(result.isValid) {
      this.ClientsService.AddClient(this.Client).subscribe(
        data => {
            this.GetAllClients();
            notify('Cliente guardado con exito', 'success', 800);
            return true;
        },
        error => {
          notify('Tuvimos un problema en almacenar la información', 'error', 800);
            return Observable.throw(error);
        }
    );
    this.modalRef.hide();

      
      
    }
  }


  GetAllClients() {
    this.ClientsService
      .getAllClients()
      .pipe(first())
      .subscribe((ClientViews) => {
        this.ClientViews = Enumerable.asEnumerable(ClientViews).OrderByDescending(u=> u.clientID).ToArray(); ;
      });
  }

  newClients(template: TemplateRef<any>) {
    this.Client = new Clients();
    this.GetAllWorkShop();
    this.modalRef = this.modalService.show(template, { class: 'modal-lg' });

  }

  GetAllWorkShop() {
    this.ClientsService
      .getAllWorkShop()
      .pipe(first())
      .subscribe((Workshops) => {
        this.Workshops = Workshops;
      });
  }

}
