﻿using APIWorkshop.Model;
using APIWorkshop.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace APIWorkshop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarsController : ControllerBase
    {

        private readonly ICarRepository _ICarRepository;
        public CarsController(ICarRepository CarRepository)
        {
            _ICarRepository = CarRepository;
        }

        // GET: api/<CarsController>
        [HttpGet]
        public IEnumerable<ViewCar> Get()
        {
            return _ICarRepository.GetAllCarForClients();
        }


        // POST api/<CarsController>
        [HttpPost]
        public HttpStatusCode Post([FromBody] Cars Item)
        {
            _ICarRepository.Add(Item);
            return HttpStatusCode.OK;
        }

        // GET api/<CarsController>/5
        [HttpGet("{id}")]
        public Detailcars Get(int id)
        {
            return _ICarRepository.GetDetails(id);
        }
    }
}
