﻿using APIWorkshop.Model;
using APIWorkshop.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace APIWorkshop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Consumes("application/json")]
    public class ClientsController : ControllerBase
    {
        private readonly ICarRepository _ICarRepository;
        private readonly IClientRepository _IClientRepository;

        public ClientsController(ICarRepository CarRepository, IClientRepository IClientRepository)
        {
            _ICarRepository = CarRepository;
            _IClientRepository = IClientRepository;
        }



        // GET: api/<ClientsController>
        [HttpGet]
        public IEnumerable<ClientView> Get()
        {
            return _IClientRepository.GetAll();
        }


        [HttpGet("GetAllWorkShop")]
        public IEnumerable<Workshop> GetAllWorkShop()
        {
            return _IClientRepository.GetAllWorkShop();
        }

        // POST api/<ClientsController>
        [HttpPost]
        public HttpStatusCode Post([FromBody] Clients Item)
        {
            _IClientRepository.Add(Item);
            return HttpStatusCode.OK;
        }

        [HttpGet("GetAllClients")]
        public IEnumerable<ViewClients> GetClients()
        {
            return _IClientRepository.GetClients();
        }
    }
}
