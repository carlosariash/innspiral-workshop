﻿using APIWorkshop.Model;
using APIWorkshop.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace APIWorkshop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RepairController : ControllerBase
    {
        private readonly IRepairRepository _IRepairRepository;

        public RepairController(IRepairRepository IRepairRepository)
        {
            _IRepairRepository = IRepairRepository;
        }

        // GET: api/<RepairController>
        [HttpGet]
        public IEnumerable<CarRepairView> Get()
        {
            return _IRepairRepository.GetAll();
        }

        // POST api/<RepairController>
        [HttpPost]
        public HttpStatusCode Post([FromBody] reparation item)
        {
            _IRepairRepository.Add(item);
            return HttpStatusCode.OK;
        }
    }
}
