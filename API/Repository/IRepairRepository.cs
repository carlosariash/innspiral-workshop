﻿using APIWorkshop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIWorkshop.Repository
{
    public interface IRepairRepository
    {
        IEnumerable<CarRepairView> GetAll();
        void Add(reparation item);
    }
}
