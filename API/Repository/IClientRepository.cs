﻿using APIWorkshop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIWorkshop.Repository
{
    public interface IClientRepository
    {
        IEnumerable<ClientView> GetAll();
        void Add(Clients items);
        IEnumerable<Workshop> GetAllWorkShop();
        IEnumerable<ViewClients> GetClients();
    }
}
