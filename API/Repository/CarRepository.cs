﻿using APIWorkshop.DBContexts;
using APIWorkshop.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIWorkshop.Repository
{
    public class CarRepository : ICarRepository
    {
        private readonly DatabaseContext _dbContext;

        public CarRepository(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }

        public void Add(Cars item)
        {
            Car _Cars = new Car();
            _Cars.ClientID = item.ClientID;
            _Cars.Maker = item.Maker;
            _Cars.Model = item.Model;
            _Cars.Year = item.Year;
            _dbContext.Cars.Add(_Cars);
            Save();
        }

        public void Modify(Car item)
        {
            _dbContext.Entry(item).State = EntityState.Modified;
            Save();
        }

        public IEnumerable<Car> GetAll()
        {
            return _dbContext.Cars.ToList();
        }

        public IEnumerable<ViewCar> GetAllCarForClients()
        {
            return _dbContext.Cars.Select(x => new ViewCar
            {
                CarID = x.CarID,
                Client = x.Client.Name + " " + x.Client.LastName,
                Maker = x.Maker,
                Model = x.Model,
                Year = x.Year
            }).ToList();
        }

        public Detailcars GetDetails(int id)
        {
            return _dbContext.Cars.Where(x => x.CarID == id).Select(x => new Detailcars
            {
                CarID = x.CarID,
                Maker = x.Maker,
                Model = x.Model,
                Year = x.Year,
                Client = x.Client.Name + " " + x.Client.LastName,
                historyReparation = x.CarRepairs.Select(y => new reparation
                {
                    carRepairID = y.CarRepairID,
                    departure = y.Departure,
                    description = y.Description,
                    entry = y.Entry



                }).ToList()


            }).FirstOrDefault();
        }
    }
}
