﻿using APIWorkshop.DBContexts;
using APIWorkshop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIWorkshop.Repository
{
    public class RepairRepository : IRepairRepository
    {
        private readonly DatabaseContext _dbContext;

        public RepairRepository(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Add(reparation item)
        {
            CarRepair _CarRepair = new CarRepair();
            _CarRepair.WorShopID = item.workShopID.Value;
            _CarRepair.Entry = item.entry.Value;
            _CarRepair.Description = item.description;
            _CarRepair.CarID = item.carID.Value;
            _CarRepair.Departure = item.departure;
            _dbContext.CarRepairs.Add(_CarRepair);
            Save();
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }

        public IEnumerable<CarRepairView> GetAll()
        {
            return _dbContext.CarRepairs.Select(x => new CarRepairView
            {
                CarRepairID = x.CarRepairID,
                Client = x.cars.Client.Name + " " + x.cars.Client.LastName,
                Departure = x.Departure,
                Description = x.Description,
                Entry = x.Entry,
                NameCar = x.cars.Model + " " + x.cars.Maker
            }).ToList();
        }
    }
}
