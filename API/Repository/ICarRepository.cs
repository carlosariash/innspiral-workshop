﻿using APIWorkshop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIWorkshop.Repository
{
    public interface ICarRepository
    {
        void Add(Cars item);
        void Modify(Car item);
        IEnumerable<Car> GetAll();
        IEnumerable<ViewCar> GetAllCarForClients();
        Detailcars GetDetails(int id);
    }
}
