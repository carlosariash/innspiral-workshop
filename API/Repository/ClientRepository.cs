﻿using APIWorkshop.DBContexts;
using APIWorkshop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIWorkshop.Repository
{
    public class ClientRepository : IClientRepository
    {
        private readonly DatabaseContext _dbContext;

        public ClientRepository(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Add(Clients items)
        {
            Client _Client = new Client();
            _Client.Name = items.Name;
            _Client.LastName = items.LastName;
            _dbContext.Clients.Add(_Client);
            Save();

            WorkshopClient _WorkshopClient = new WorkshopClient();
            _WorkshopClient.ClientID = _Client.ClientID;
            _WorkshopClient.WorkShopID = items.WorkShopID;

            _dbContext.WorkshopClients.Add(_WorkshopClient);
            Save();



        }

        public IEnumerable<ClientView> GetAll()
        {
            return _dbContext.WorkshopClients.Select(x => new ClientView
            {
                WorkshopClientID = x.WorkshopClientID,
                Name = x.Client.Name,
                LastName = x.Client.LastName,
                ClientID = x.ClientID,
                WorkShop = x.Workshops.Name
            }).ToList();
        }

        public IEnumerable<Workshop> GetAllWorkShop()
        {
            return _dbContext.Workshops.Select(x => new Workshop { Name = x.Name, WorkShopID = x.WorkShopID }).ToList();
        }

        public IEnumerable<ViewClients> GetClients()
        {
            return _dbContext.Clients.Select(x => new ViewClients
            {
                ClientID = x.ClientID,
                FullName = x.Name + " " + x.LastName
            }).ToList();
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }
    }
}
