﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace APIWorkshop.Model
{
    public class Car
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CarID { get; set; }
        public string Model { get; set; }
        public string Maker { get; set; }
        public int? Year { get; set; }

        [ForeignKey("Clients")]
        public int ClientID { get; set; }
        public virtual Client Client { get; set; }
        public virtual List<CarRepair> CarRepairs { get; set; }

    }


    public class ViewCar
    {
        public int CarID { get; set; }
        public string Model { get; set; }
        public string Maker { get; set; }
        public string Client { get; set; }
        public int? Year { get; set; }
    }


    public class Cars
    {
        public string Model { get; set; }
        public string Maker { get; set; }
        public int? Year { get; set; }
        public int ClientID { get; set; }
    }


    public class Detailcars : ViewCar
    {



        public List<reparation> historyReparation { get; set; }
    }


    public class reparation
    {
        public int carRepairID { get; set; }
        public DateTime? departure { get; set; }
        public string description { get; set; }
        public DateTime? entry { get; set; }
        public int? carID { get; set; }

        public int? workShopID { get; set; }
    }
}
