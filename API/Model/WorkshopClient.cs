﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace APIWorkshop.Model
{
    public class WorkshopClient
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkshopClientID { get; set; }

        [ForeignKey("Clients")]
        public int ClientID { get; set; }

        [ForeignKey("Workshops")]
        public int WorkShopID { get; set; }
        public virtual Workshop Workshops { get; set; }
        public virtual Client Client { get; set; }
    }
}
