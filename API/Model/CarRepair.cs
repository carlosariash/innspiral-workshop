﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace APIWorkshop.Model
{
    public class CarRepair
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CarRepairID { get; set; }
        public string Description { get; set; }
        public DateTime Entry { get; set; }
        public DateTime? Departure { get; set; }

        [ForeignKey("Workshops")]
        public int WorShopID { get; set; }

        [ForeignKey("Cars")]
        public int CarID { get; set; }
        public virtual Workshop Workshops { get; set; }
        public virtual Car cars { get; set; }


    }

    public class CarRepairView
    {

        public int CarRepairID { get; set; }
        public string Description { get; set; }
        public DateTime Entry { get; set; }
        public DateTime? Departure { get; set; }

        public string NameCar { get; set; }

        public string Client { get; set; }



    }


}
