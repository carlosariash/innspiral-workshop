﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace APIWorkshop.Model
{
    public class Client
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClientID { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public virtual WorkshopClient WorkshopClients { get; set; }
        public virtual List<Car> Cars { get; set; }
    }


    public class Clients
    {

        public string Name { get; set; }
        public string LastName { get; set; }
        public int WorkShopID { get; set; }
    }

    public class ViewClients
    {
        public int ClientID { get; set; }
        public string FullName { get; set; }
    }



    public class ClientView
    {

        public int WorkshopClientID { get; set; }
        public int ClientID { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }

        public string WorkShop { get; set; }


    }
}
