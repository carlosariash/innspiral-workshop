﻿using APIWorkshop.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIWorkshop.DBContexts
{
    public class DatabaseContext: DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }
        public DbSet<Car> Cars { get; set; }
        public DbSet<CarRepair> CarRepairs { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<WorkshopClient> WorkshopClients { get; set; }
        public DbSet<Workshop> Workshops { get; set; }
    }
}
